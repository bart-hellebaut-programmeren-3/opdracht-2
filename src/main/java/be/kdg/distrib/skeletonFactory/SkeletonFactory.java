package be.kdg.distrib.skeletonFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class SkeletonFactory {
    public static Skeleton createSkeleton(Object object){
        InvocationHandler handler = new SkeletonInvocationHandler(object);
        return (Skeleton) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{Skeleton.class}, handler);
    }
}
