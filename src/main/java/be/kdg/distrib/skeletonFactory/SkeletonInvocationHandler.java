package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.*;

public class SkeletonInvocationHandler implements InvocationHandler {
    private final MessageManager messageManager;
    private final Object objectToImplement;

    public SkeletonInvocationHandler(Object objectToImplement) {
        this.objectToImplement = objectToImplement;
        this.messageManager = new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("getAddress")){
            return messageManager.getMyAddress();
        }
        if (method.getName().equals("handleRequest")){
            MethodCallMessage request = (MethodCallMessage) args[0];
            handleRequests(request);
        }
        if (method.getName().equals("run")){
            run();
        }
        return null;
    }


    private void handleRequests(MethodCallMessage request) throws Throwable {
        Class<?> myObjectClass = objectToImplement.getClass();
        String methodName = request.getMethodName();
        Map<String, String> parameters = request.getParameters();
        Method[] methodes = myObjectClass.getMethods();
        Optional<Method> methode = Arrays.stream(methodes).filter(method1 -> method1.getName().equals(methodName)).findAny();
        Class methodReturnType = methode.get().getReturnType();

        //parameters groeperen adhv van nummer achter arg => value ervan in lijst steken.
        Object[] messageValues = new Object[methode.get().getParameters().length];


        Iterator it = parameters.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (!pair.getKey().toString().startsWith("arg")) {
                throw new RuntimeException();
            } else {
                //nummer net na arg eruit halen
                int argNr = Integer.parseInt(String.valueOf(pair.getKey().toString().charAt(3)));

                if (pair.getKey().toString().contains(".")){
                    //als deel van object
                    String value = pair.getValue().toString();

                    List<Object> values;
                    if (messageValues[argNr] == null){
                        values = new ArrayList<>();
                    } else {
                        values = (List<Object>) messageValues[argNr];
                    }

                    if (tryParseInt(value)){
                        int i = Integer.parseInt(value);
                        values.add(i);
                    } else if (tryParseDouble(value)){
                        values.add(Double.parseDouble(value));
                    } else if (tryParseChar(value)){
                        values.add(value.charAt(0));
                    } else if (tryParseBoolean(value)){
                        values.add(Boolean.parseBoolean(value));
                    } else {
                        values.add(value);
                    }
                    messageValues[argNr] = values;

                } else {
                    //primitief en gewoon aan lijst toevoegen
                    String value = pair.getValue().toString();

                    if (tryParseInt(value)){
                        int i = Integer.parseInt(value);
                        messageValues[argNr] = i;
                    } else if (tryParseDouble(value)){
                        messageValues[argNr] = Double.parseDouble(value);
                    } else if (tryParseChar(value)){
                        messageValues[argNr] = value.charAt(0);
                    } else if (tryParseBoolean(value)){
                        messageValues[argNr] = Boolean.parseBoolean(value);
                    } else {
                        messageValues[argNr] = value;
                    }
                }
            }
        }

        //alle lijsten binnen de lijst omzetten in een object
        for (int i = 0; i < messageValues.length; i++) {
            if (messageValues[i].getClass().equals(ArrayList.class)){
                List<Object> values = (List<Object>) messageValues[i];

                //object aanmaken
                Class objectClass = methode.get().getParameters()[i].getType();

                Object o = null;
                if (objectClass != null) {
                    o = objectClass.getDeclaredConstructor().newInstance();
                }

                //fields invullen
                Field[] fields = objectClass.getDeclaredFields();
                for (Field field : fields) {
                    field.setAccessible(true);
                    Iterator it2 = parameters.entrySet().iterator();
                    while (it2.hasNext()) {
                        Map.Entry pair = (Map.Entry) it2.next();
                        String fieldName = pair.getKey().toString().substring(pair.getKey().toString().lastIndexOf(".") + 1);
                        if (fieldName.equals(field.getName())) {
                            if (tryParseInt(String.valueOf(pair.getValue()))) {
                                field.set(o, Integer.parseInt(String.valueOf(pair.getValue())));
                            } else if (tryParseDouble(String.valueOf(pair.getValue()))) {
                                field.set(o, Double.parseDouble(String.valueOf(pair.getValue())));
                            } else if (tryParseBoolean(String.valueOf(pair.getValue()))) {
                                field.set(o, Boolean.parseBoolean(String.valueOf(pair.getValue())));
                            } else if (tryParseChar(String.valueOf(pair.getValue()))) {
                                field.set(o, String.valueOf(pair.getValue()).charAt(0));
                            } else {
                                field.set(o, pair.getValue());
                            }
                        }
                    }
                }

                //object terug in lijst steken
                messageValues[i] = o;
            }
        }

        //method invocation
        Object returned = null;

        if (parameters.isEmpty()){
            returned = methode.get().invoke(objectToImplement);
        } else {
            returned = methode.get().invoke(objectToImplement, messageValues);
        }

        //reply
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        if (methodReturnType.equals(Void.TYPE)){
            reply.setParameter("result", "Ok");
        } else if(methodReturnType == int.class || methodReturnType == String.class || methodReturnType == boolean.class || methodReturnType == char.class || methodReturnType == Integer.class) {
            if (parameters.isEmpty()){
                reply.setParameter("result", String.valueOf(returned));
            } else {
                for (String value: parameters.values()) {
                    reply.setParameter("result."+value, String.valueOf(returned));
                }
            }
        } else {
            for (Field field : returned.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                reply.setParameter("result."+field.getName(), field.get(returned).toString());
            }
        }
        messageManager.send(reply, request.getOriginator());
    }

    private void listen(){
        while(true){
            MethodCallMessage request = messageManager.wReceive();
            try {
                handleRequests(request);
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    private void run(){
        Thread thread = new Thread(this::listen);
        thread.start();
    }

    private boolean tryParseInt(String value){
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    private boolean tryParseDouble(String value){
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    private boolean tryParseBoolean(String value){
        return value.equals("true") || value.equals("false");
    }

    private boolean tryParseChar(String value){
        return value.length() == 1;
    }

    private Class getPrimitiveClass(String value) throws Throwable {
        if (value.contains("String")){
            return String.class;
        }
        String className = value.replace("class ", "");
        return (Class) Class.forName(className).getField("TYPE").get(null);
    }
}
