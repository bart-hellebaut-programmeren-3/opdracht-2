package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;

public class StubInvocationHandler implements InvocationHandler {
    private final MessageManager messageManager;
    private final NetworkAddress networkAddress;

    public StubInvocationHandler(String ip, int port) {
        this.messageManager = new MessageManager();
        this.networkAddress = new NetworkAddress(ip, port);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), methodName); //message met daarin de naam van de methode

        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                Class<?> argClass = args[i].getClass();
                if (argClass == int.class || argClass == Double.class || argClass == String.class || argClass == Boolean.class || argClass == Character.class || argClass == Integer.class){
                    message.setParameter("arg"+i, String.valueOf(args[i])); //voor elke parameter een setParameter met de data in.
                } else { //als de parameter niet primitive is (aka int, double, char, etc)
                    Field[] fields = argClass.getDeclaredFields();
                    for (int j = 0; j < fields.length; j++) {
                        Field field = fields[j];
                        field.setAccessible(true);
                        String fieldName = field.getName();
                        message.setParameter(
                                "arg"+i+"."+fieldName,
                                String.valueOf(field.get(args[i]))
                        );
                    }
                }
            }
        }
        messageManager.send(message, networkAddress);

        MethodCallMessage reply = messageManager.wReceive();
        Class<?> returnType = method.getReturnType();
        if (returnType.equals(String.class)) return reply.getParameter("result");
        if (returnType.equals(int.class)) return Integer.parseInt(reply.getParameter("result"));
        if (returnType.equals(Integer.class)) return Integer.parseInt(reply.getParameter("result"));
        if (returnType.equals(boolean.class)) return Boolean.parseBoolean(reply.getParameter("result"));
        if (returnType.equals(char.class)) return reply.getParameter("result").charAt(0);
        if (!returnType.isPrimitive() && !returnType.equals(String.class)){
            Object object = method.getReturnType().getDeclaredConstructor().newInstance();
            Iterator it = reply.getParameters().entrySet().iterator();
            while(it.hasNext()){
                Map.Entry pair = (Map.Entry)it.next();
                String fieldName = pair.getKey().toString();
                fieldName = fieldName.replace("result.", "");
                Field field = method.getReturnType().getDeclaredField(fieldName);
                field.setAccessible(true);
                if (field.getType().equals(String.class)){
                    field.set(object, reply.getParameter("result."+field.getName()));
                }
                if (field.getType().equals(int.class)){
                    field.set(object, Integer.parseInt(reply.getParameter("result."+field.getName())));
                }
                if (field.getType().equals(boolean.class)){
                    field.set(object, Boolean.parseBoolean(reply.getParameter("result."+field.getName())));
                }
                if (field.getType().equals(char.class)){
                    field.set(object, reply.getParameter("result."+field.getName()).charAt(0));
                }
                if (field.getType().equals(Integer.class)){
                    field.set(object, Integer.parseInt(reply.getParameter("result."+field.getName())));
                }
            }

            return object;
        }
        return null;
    }
}
